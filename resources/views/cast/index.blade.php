@extends('layout.master')

@section('judul')
	list caster Film
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success btn-sm mb-3" >Tambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">Biografi</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>

                <td>

                <form action="/cast/{{$item->id}}" method="POST">
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm" >edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn-danger btn-sm" value="delete">
                </form>
               
                </td>
                
            </tr>
        @empty
            <tr>
                <td>
                    data masih kosong
                </td>
            </tr>
        @endforelse
    </tbody>
  </table>


  @endsection