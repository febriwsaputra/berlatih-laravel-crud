@extends('layout.master')

@section('judul')
	edit caster Film {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label class="form-label">Cast name</label>
      <input type="text" name="nama" value="{{$cast->nama}}" >
    </div>

    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Umur</label>
        <input type="number" name="umur" value="{{$cast->umur}}">
      </div>
  
      @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="mb-3">
      <label class="form-label">Biografi</label>
        <textarea name="bio" class="form-control"  cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

